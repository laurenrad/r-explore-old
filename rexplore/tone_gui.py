# R-Explorer: tone_viewer.py
# Tone detail view window

import os
import tkinter as tk
from tkinter import ttk
from tkinter.filedialog import asksaveasfilename
import rexplore.formatting as fmt

import rexplore.widgets as w
import rexplore.app as app


class ParamsTab(tk.Frame):
    """Create a tab to display parameters specified in a list"""
    def __init__(self,parent,tone,param_list,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)

        params_frame = w.ParamsFrame(self)
        params_frame.grid_columnconfigure(0,weight=1,minsize=100)
        
        for key,value in tone.values.items():
            if key in param_list:
                if key == 'SOURCE TONE':
                    if tone.values['ORIG/SUB TONE'] == 1: # if this is a subtone,
                        params_frame.add(key,value) # add Source Tone
                    else: # otherwise, indicate
                        params_frame.add('Original Tone', '***')
                else:
                    params_frame.add(key,value)
                
        #params_frame.grid(row=0,column=0,sticky=(tk.W + tk.E))
        params_frame.pack(fill=tk.BOTH, expand=1, padx=5)
        
        # Envelope views
        # Make this better later
        if param_list is fmt.Fields550.tone_params_tvf:
            sustain = tone.values['TVF ENV SUSTAIN POINT']
            end = tone.values['TVF ENV END POINT']
            env = tone.values['TVF ENV']
            w.EnvelopeView(self,sustain,end,env).pack(fill=tk.BOTH, expand=1, padx=5)
        elif param_list is fmt.Fields550.tone_params_tva:
            sustain = tone.values['TVA ENV SUSTAIN POINT']
            end = tone.values['TVA ENV END POINT']
            env = tone.values['TVA ENV']            
            w.EnvelopeView(self,sustain,end,env).pack(fill=tk.BOTH, expand=1, padx=5)

            
# Tab box for organizing and displaying tone parameters
class ToneTabBox(ttk.Notebook):
    """Displays information about a single tone in a tabbed view."""
    def __init__(self,parent,tone,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)

        # Create tabs for each category defined in ToneFields and add them
        self.add(ParamsTab(self,tone,fmt.Fields550.tone_params_main),text='Main') # Main tab
        self.add(ParamsTab(self,tone,fmt.Fields550.tone_params_loop),text='Loop') # Loop tab
        self.add(ParamsTab(self,tone,fmt.Fields550.tone_params_lfo),text='LFO') # LFO tab
        self.add(ParamsTab(self,tone,fmt.Fields550.tone_params_tvf),text='TVF') # TVF tab
        self.add(ParamsTab(self,tone,fmt.Fields550.tone_params_tva),text='TVA') # TVA tab

# Window for viewing details of one tone at a time
class ToneViewer(tk.Toplevel):
    """Window for displaying tone details."""
    def __init__(self,number,*args,**kwargs):
        super().__init__(*args,**kwargs)
        
        # create a window and just dump the raw parameter info on it
        self.tone = app.get_disk().tones[number]
        self.current_num = number # for export and future navigation options

        # Set window properties
        self.resizable(width=False,height=False)
        self.title("Tone View")

        # Create the tab box which holds the different categories of parameter views
        self.tab_box = ToneTabBox(self,self.tone)
        self.tab_box.pack(pady=5)
        
        # export button
        export_button = tk.Button(self,text="Export Wave",
                                  command=self._single_export)
        export_button.pack(side=tk.RIGHT,padx=5,pady=5)
        if self.tone.length() == 0:
            export_button.config(state='disabled') # disable button if tone is empty
            
        tk.Button(self,text="Close",command=self.destroy).pack(side=tk.LEFT,padx=5,pady=5)
        
    def __del__(self):
        print("TEST")

    def _single_export(self):
        """Export wave data for a single tone."""
        default = self.tone.get_name().strip() + '.wav'
        filename = asksaveasfilename(initialdir=app.home_path,
                                     initialfile=default,
                                     defaultextension='.wav',
                                     filetypes = (("WAV (16-bit PCM)", "*.wav"),),
                                     title="Choose a file")
        
        if filename != '':
            app.export_tone(self.current_num,filename)