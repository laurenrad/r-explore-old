"""
R-Explore: widgets.py
This module contains custom widget classes for use by the R-Explore GUI.
"""
import tkinter as tk
from tkinter import ttk
#from rexplore.formatting import Fields550 # Display formatting definitions
#from rexplore.formatting import display_names # Display names for parameters
import rexplore.formatting as formatting

font_bold = 'TkDefaultFont 10 bold'

class ParamsFrame(tk.Frame):
    """A single row for displaying a parameter name and value."""
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.rows = 1 # number of rows added
    
    def add(self, key, value):

        self.pname = tk.StringVar() # parameter name
        self.pname.set(formatting.display_names.get(key,key)) # default to key name if no display name
        self.pvalue = tk.StringVar() # parameter value
        if key in formatting.Fields550.bindings:
            try:
                # use formatting if available
                self.pvalue.set(str(formatting.Fields550.bindings[key](value))) 
            except Exception as E:
                print(f"Caught exception '{E}' while trying to format value '{value}' for '{key}'")
        else:
            self.pvalue.set(str(value))
        
        key_label = tk.Label(self,textvariable=self.pname)
        key_label.config(font='TkDefaultFont 10 bold')
        key_label.grid(row=self.rows,column=0,sticky=tk.W)
        value_label = tk.Label(self,textvariable=self.pvalue)
        value_label.grid(row=self.rows,column=1,sticky=tk.E)
        self.rows += 1 # increment row

class PerChannelFrame(tk.LabelFrame):
    """LabelFrame for displaying per channel (A-H) parameters."""
    def __init__(self, parent, header=['A','B','C','D','E','F','G','H'], *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        
        self.rows = 1 # number of rows added
        # create table header
        tk.Label(self,text=' ',borderwidth=2,
                 relief="ridge").grid(row=self.rows,
                                      column=0,sticky=(tk.E + tk.W)) # empty upper left cell
        for (col,text) in enumerate(header):
            h = tk.Label(self,text=text,borderwidth=2,relief="ridge")
            h.config(font=font_bold)
            h.grid(row=self.rows,column=col+1,sticky=(tk.E + tk.W))
        self.rows += 1
            
    def add_row(self, values: tuple, label: str):
        """Add a table row created from a tuple of 8 values to a grid."""
        p = tk.Label(self,text=label,borderwidth=2,relief="ridge") # param name label
        p.config(font=font_bold) # set bold font
        p.grid(row=self.rows,column=0,sticky=(tk.E + tk.W))
        
        for (index, item) in enumerate(values):
            tk.Label(self,text=str(item),
                     borderwidth=2,relief="ridge").grid(row=self.rows,column=index+1,
                                                        sticky=(tk.E + tk.W))
        self.rows += 1
            
class EnvelopeView(tk.Frame):
    """Frame class for displaying envelope data."""
    def __init__(self, parent, sus, end, env, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        bold = 'TkDefaultFont 10 bold'
        
        # Create table of envelope values
        # env = level,rate x8
        # split apart into separate rate and level lists
        rate = []; level = []
        for i in range(0,len(env),2):
            level.append(env[i])
            rate.append(env[i+1])
        env_table = PerChannelFrame(self,header=[str(i) for i in range(1,9)],text='Envelope')
        env_table.add_row(rate,'Rate')
        env_table.add_row(level,'Level')
        env_table.grid(row=1,column=0,columnspan=8,sticky=(tk.W + tk.E))
        
        # Create sustain and end labels
        self.sustain = tk.StringVar() # Envelope sustain value
        self.endpoint = tk.StringVar() # Envelope end point
        # this is coded to TVF but the binding is the same
        self.sustain.set(str(formatting.Fields550.bindings['TVF ENV SUSTAIN POINT'](sus)))
        self.endpoint.set(str(formatting.Fields550.bindings['TVF ENV END POINT'](end)))
        sus_label = tk.Label(env_table,text='Sus:')
        sus_label.config(font=bold)
        sus_label.grid(row=0,column=1,columnspan=2,sticky=tk.W)
        tk.Label(env_table,textvariable=self.sustain).grid(row=0,column=3,
                                                      columnspan=2,sticky=tk.W)
        end_label = tk.Label(env_table,text='End:')
        end_label.config(font=bold)
        end_label.grid(row=0,column=5,columnspan=2,sticky=tk.W)
        tk.Label(env_table,textvariable=self.endpoint).grid(row=0,column=7,
                                                       columnspan=2,sticky=tk.W)
        
class DiskLabelFrame(tk.LabelFrame):
    """LabelFrame class for displaying a disk label."""
    def __init__(self, parent, label_raw: bytes, *args, **kwargs):
        super().__init__(parent, text="Disk Label", *args, **kwargs)
        
        label = formatting.readable_disk_label(label_raw)
        for row_index, line in enumerate(label.splitlines()):
            tk.Label(self, text=line).grid(row=row_index,sticky=tk.W)
            
class ProgressWindow(tk.Toplevel):
    """Modal dialog to display progress, mainly for multiple sample export."""
    def __init__(self, text='Exporting samples...', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.geometry("220x90")
        self.resizable(width=False,height=False)
        self.protocol("WM_DELETE_WINDOW", self._intercept_close)
        self.status_text = tk.StringVar()
        self.status_text.set(text)
        tk.Label(self,textvariable=self.status_text).pack()
        self.progress = ttk.Progressbar(self, orient = tk.HORIZONTAL, 
              length = 200, mode = 'determinate')
        self.progress.pack()
        
    def update_progress(self, *, steps=10):
        """
        Callback to update the progress bar.
        Steps is the number of steps the action is expected to take.
        """
        increment = 100 / steps # amount to increment by
        self.progress['value'] += increment
        self.update()
        
        if self.progress['value'] >= 100:
            self.status_text.set("Action complete.")
            tk.Button(self,text="Done",command=self.destroy).pack(pady=10)
            
    def _intercept_close(self):
        """Intercept window close function."""
        pass
        