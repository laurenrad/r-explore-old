"""
R-Explore: main_gui.py
This module creates the main GUI window for R-Explore.
"""
import _thread as thread
import os

import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename

# package specific imports
import rexplore.app as app
import rexplore.formatting as formatting
import rexplore.widgets as w
import rexplore.menus as menus
import rexplore.formats
from .images import * # window icon
from .doc import README # README file
from rexplore.tone_gui import ToneViewer # Tone details window
from rexplore.patch_gui import PatchViewer # Patch details window
from rexplore.exceptions import *

class PatchListItem(tk.Frame):
    """Wrapper class for a single patch entry on the list of patches."""
    # contains label of patch and name, and a button for opening the patch's
    # parameter info. must be passed a disk image and patch number (0-15)
    # Largely the same as ToneListItem but not playable, may combine later
    def __init__(self,parent,number,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)
        self.number = number # save the patch number for button handling
        self.name = app.get_disk().patches[self.number].get_name()
        disk = app.get_disk() # get disk reference
        if type(disk.format) is rexplore.formats.S50:
            # S-50 only has 8 patches, so this is simpler
            patch_label = ('P' + str(self.number+1) + ' ' + self.name)
        else:
            patch_label = (formatting.roland_number(self.number) + ' ' + self.name)

        tk.Label(self,text=patch_label,anchor=tk.W,width=16).grid(row=0,column=0)
        
        if type(app.get_disk().format) is not rexplore.formats.S50:
            # Until things can be reworked, this is disabled for S-50.
            self.info_icon = tk.PhotoImage(file=ICON_INFO)
            tk.Button(self,text="Info",image=self.info_icon, compound=tk.LEFT, width=45,
                      command=self.on_info).grid(row=0,column=2) # leave a blank column for later
            
    
    def on_info(self):
        """Open a PatchViewer window the the selected patch."""
        PatchViewer(self.number)

class PatchListFrame(tk.Frame):
    """Frame for displaying a list of patches and operations"""
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        rows = app.get_disk().format.PATCH_COUNT // 2 # display patches in 2 rows
        
        # left column (8 patches)
        for i in range(0,rows):
            PatchListItem(self,i).grid(row=i,column=0,sticky=tk.W)
        # right column (8 patches)
        for i in range(rows,rows*2):
            PatchListItem(self,i).grid(row=i-rows,column=1,sticky=tk.E)

class ToneListItem(tk.Frame):
    """Wrapper class for a single tone entry on the list of tones."""
    # contains label of tone number and name, and buttons for playing
    # the tone's wave data and viewing the tone parameters
    # must be passed a disk image and a tone number (0-31)
    def __init__(self,parent,num,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)
        tone = app.get_disk().tones[num]
        self.number = num # save the tone number for button handling
        self.name = tone.get_name() # get tone name
        self.empty = (tone.length() == 0)
        
        tone_label = (formatting.roland_number(self.number) + ' '
                      + self.name)
        tk.Label(self,text=tone_label,anchor=tk.W).pack(side=tk.LEFT)
        
        # If S-50, just replace Tone Viewer button with an export button
        if type(app.get_disk().format) is rexplore.formats.S50:
            self.export_icon = tk.PhotoImage(file=ICON_EXPORT)
            tk.Button(self,text="Export",image=self.export_icon, width=60,
                      compound=tk.LEFT,command=self._on_export).pack(side=tk.RIGHT)
        else:
            self.info_icon = tk.PhotoImage(file=ICON_INFO)
            tk.Button(self,text="Info", image=self.info_icon,
                      compound=tk.LEFT,width=45, command=self._on_info).pack(side=tk.RIGHT)
        
        self.play_icon = tk.PhotoImage(file=ICON_PLAY)
        play_button = tk.Button(self,text="Play",command=self._on_play,
                                image=self.play_icon,compound=tk.LEFT,width=45)
        play_button.pack(side=tk.RIGHT)
        if self.empty:
            play_button.config(state='disabled')
            

    def _on_play(self):
        # don't play empty tones because it'll just play the last tmp file
        if not self.empty:
            app.play(self.number)

    
    def _on_info(self):
        """Callback handler for displaying Tone Viewer window."""
        tone_window = ToneViewer(self.number)
        #tone_window.mainloop()
        
    def _on_export(self):
        """
        Callback for exporting a tone's wave data. This exists because the
        S-50 requires a stripped down GUI for now, so Tone Viewer is disabled.
        """
        """Export wave data for a single tone."""
        default = self.name + '.wav'
        filename = asksaveasfilename(initialdir=app.home_path,
                                     initialfile=default,
                                     defaultextension='.wav',
                                     filetypes = (("WAV (16-bit PCM)", "*.wav"),),
                                     title="Choose a file")
        
        if filename != '':
            app.export_tone(self.number,filename)

class ToneListFrame(tk.Frame):
    """Frame for displaying a list of tones and operations"""
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        # left column (16 tones)
        for i in range(0,16):
            ToneListItem(self,i).grid(row=i,column=0,sticky=(tk.W+tk.E))
        # right column (16 tones)
        for i in range(16,32):
            ToneListItem(self,i).grid(row=i-16,column=1,sticky=(tk.W+tk.E))

           
class FunctionFrame(tk.Frame):
    """Frame for displaying function parameters"""
    # constructor takes reference to root which holds disk instance
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        disk = app.get_disk() # get disk reference
        

        vals = app.get_disk().function.values
        
        # Create label frame with disk label text left justified
        w.DiskLabelFrame(self, vals["DISK LABEL"]).pack(fill=tk.BOTH, expand=1, padx=5)
              
        # Add frame for single parameters
        params_frame = w.ParamsFrame(self)
        params_frame.add('MASTER TUNE',vals['MASTER TUNE'])
        params_frame.add('KEYBOARD DISPLAY',vals['KEYBOARD DISPLAY'])
        params_frame.add('VOICE MODE',vals['VOICE MODE'])
        params_frame.add('EXTERNAL CONTROLLER',vals['EXTERNAL CONTROLLER'])
        params_frame.pack(fill=tk.BOTH, expand=1, padx=5)
        
        # Create a table for parameters which apply per play channel
        # get references to parameter values and format for display
        # this should be more efficient
        formatted_vals = {}
        for (key,value) in disk.function.values.items():
            if key in formatting.Fields550.bindings:
                formatted_vals[key] = formatting.Fields550.bindings[key](value)
            else:
                formatted_vals[key] = value # just copy over values with no binding defined
        multi_frame = w.PerChannelFrame(self,text='Play')
        multi_frame.add_row(formatted_vals['MULTI MIDI RX-CH'], 'RX-CH')
        multi_frame.add_row(formatted_vals['MULTI PATCH NUMBER'], 'Patch')
        multi_frame.add_row(formatted_vals['MULTI LEVEL'], 'Level')
        params_frame.grid_columnconfigure(0,weight=1) # grow columns
        multi_frame.pack(fill=tk.BOTH, expand=1, padx=5)

class MidiFrame(tk.Frame):
    """Frame for displaying MIDI parameters"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        disk = app.get_disk() # get disk reference
        
        # get references to MIDI parameter values and format for display
        vals = {}
        for (key,value) in disk.midi.values.items():
            if key in formatting.Fields550.bindings:
                vals[key] = formatting.Fields550.bindings[key](value)
            else:
                vals[key] = value # just copy over values with no binding defined
        
        # table to display all parameters that apply to a specific Play channel
        multi_frame = w.PerChannelFrame(self, text="MIDI-Message")
        # add table rows
        multi_frame.add_row(vals["RX CHANNEL"],
                            formatting.display_names.get('RX CHANNEL','RX-CH:'))
        multi_frame.add_row(vals["RX PROGRAM CHANGE"],
                            formatting.display_names.get('RX PROGRAM CHANGE','P.Chg: '))
        multi_frame.add_row(vals["RX BENDER"],
                            formatting.display_names.get('RX BENDER','Bend: '))
        multi_frame.add_row(vals["RX BEND RANGE"],
                            formatting.display_names.get('RX BENDER BENDER','B. Rng: '))
        multi_frame.add_row(vals["RX MODULATION"],
                            formatting.display_names.get('RX MODULATION','Mod: '))
        multi_frame.add_row(vals["RX HOLD"],
                            formatting.display_names.get('RX HOLD','Hold: '))
        multi_frame.add_row(vals["RX AFTER TOUCH"],
                            formatting.display_names.get('RX AFTER TOUCH','A.T: '))
        multi_frame.add_row(vals["RX VOLUME"],
                            formatting.display_names.get('RX VOLUME','Vol: '))
        multi_frame.pack(fill=tk.BOTH, expand=1, padx=5)

        # RX Program Change num
        progchange_frame = w.PerChannelFrame(self, header=[str(i) for i in range(1,9)], text="MIDI-Prog #")
        progchange_frame.add_row(vals["RX PROGRAM CHANGE NUMBER"][0:8],'I1')
        progchange_frame.add_row(vals["RX PROGRAM CHANGE NUMBER"][8:16],'I2')
        progchange_frame.add_row(vals["RX PROGRAM CHANGE NUMBER"][16:24],'II1')
        progchange_frame.add_row(vals["RX PROGRAM CHANGE NUMBER"][24:32],'II2')
        progchange_frame.pack(fill=tk.BOTH, expand=1, padx=5)
        
        # global params
        v = app.get_disk().midi.values # get unformatted values
        params_frame = w.ParamsFrame(self)
        params_frame.add('SYSTEM EXCLUSIVE',v['SYSTEM EXCLUSIVE'])
        params_frame.add('DEVICE ID',v['DEVICE ID'])
        #params_frame.grid(row=5,sticky=(tk.W + tk.E),fill=tk.BOTH, expand=1)
        
        
        params_frame.grid_columnconfigure(0,weight=1) # grow columns
        params_frame.pack(fill=tk.BOTH, expand=1, padx=5)

class TabBox(ttk.Notebook):
    """Tabbed browsing view for disk data"""
    def __init__(self,parent,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)
        
        if app.disk_is_empty():
            # if no disk is open, create an empty tab box
            self._create_empty()
        else:
            # otherwise, create a tab box with its contents
            
            # The S-50 has a drastically different interface, so until a lot of
            # the program can be reworked, in order to still provide core
            # functionality, there will be a drastically stripped down interface
            # for S-50 disks to just catalog and allow export.
        
            fmt = app.get_disk().format
            if type(fmt) is rexplore.formats.S50:
                # S-50 interface
                self.function_tab = w.DiskLabelFrame(self,app.get_disk_label()) # just add the disk label directly
                self.midi_tab = tk.Frame() # no midi tab, but create one for the cleanup method
                self.patch_tab = PatchListFrame() # this will also alter itself for S-50
                self.tone_tab = ToneListFrame() # this will also alter itself for S-50
                self.add(self.function_tab,text='Disk')
                self.add(self.patch_tab,text='Patches')
                self.add(self.tone_tab,text='Tones')
            else:
                # S-550, S-330 interface
                self.function_tab = FunctionFrame()
                self.midi_tab = MidiFrame()
                self.patch_tab = PatchListFrame()
                self.tone_tab = ToneListFrame()
                self.add(self.function_tab,text='Function')
                self.add(self.midi_tab,text='MIDI')
                self.add(self.patch_tab,text='Patches')
                self.add(self.tone_tab,text='Tones')
            

        
    def _create_empty(self):
        """Create an empty tabbed view if no file is open."""
        # Create empty frames so they can be destroyed when a file is opened
        self.function_tab = tk.Frame()
        self.midi_tab = tk.Frame()
        self.patch_tab = tk.Frame()
        self.tone_tab = tk.Frame()
        self.add(self.function_tab,text='Function',state='disabled')
        self.add(self.midi_tab,text='MIDI',state='disabled')
        self.add(self.patch_tab,text='Patches',state='disabled')
        self.add(self.tone_tab,text='Tones',state='disabled')
        
    def enable(self):
        """Enable all tabs and select the first tab."""
        for tab in self.tabs():
            self.tab(tab,state='normal')
        self.select(0)
        
    def disable(self):
        """Disable all tabs."""
        for tab in self.tabs():
            self.tab(tab,state='disabled')
        
    def cleanup(self):
        """
        There seems to be an issue with the tabs being destroyed after closing a tab box,
        which leads to heavy memory leaks. Calling this when opening a new file is a
        temporary solution.
        """
        self.function_tab.destroy()
        self.midi_tab.destroy()
        self.patch_tab.destroy()
        self.tone_tab.destroy()
        
class AboutWindow(tk.Toplevel):
    """Custom About dialog."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title("About R-Explore")
        self.resizable(width=False,height=False)
        self.description = tk.StringVar() # Main description
        self.description.set('Sample disk browser for Roland 12-bit samplers.')
        self.version = tk.StringVar() # StringVar of version
        self.version.set('Version ' + app.version)
        self.body = tk.StringVar() # Other body text
        self.body.set('Copyright 2020 Lauren Croney\n' + 'This program comes with NO WARRANTY ' +
                      'and is released under the terms of the MIT License. ' +
                      'See LICENSE file for more information, or ' +
                      'https://opensource.org/licenses/MIT \n')
        self.icon = tk.PhotoImage(file=ICON_48) # Program icon
        wrap = 250 # word wrap length for text
        
        tk.Label(self,image=self.icon).pack()
        tk.Label(self,textvariable=self.description,wraplength=wrap).pack()
        tk.Label(self,textvariable=self.version,justify=tk.LEFT).pack()
        tk.Label(self,textvariable=self.body,justify=tk.LEFT,wraplength=wrap).pack()
        tk.Button(self,text='Close',command=self.destroy).pack(side=tk.RIGHT)
        
    def _on_license(self):
        pass
                
            
class HelpViewer(tk.Toplevel):
    """Help viewer window."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.title("R-Explore: Help Viewer")
        self.resizable(width=False,height=False)
        text = open(README).read() # just read in the readme
        
        # Text box - change the background so it's obvious you can't enter text.
        self.text_box = tk.Text(self,wrap=tk.WORD,bg='#D9D9D9')
        self.text_box.insert('1.0',text)
        self.text_box.configure(state='disabled') # best solution to make read-only for now
        
        # Add scrollbar
        self.scrollbar = ttk.Scrollbar(self,orient=tk.VERTICAL,command=self.text_box.yview)   
        
        self.scrollbar.grid(row=0,column=1,sticky='NSW')
        self.text_box.configure(yscrollcommand=self.scrollbar.set)
        self.text_box.grid(row=0,column=0)
        
        # Add ok button
        tk.Button(self,text="Ok",command=self.destroy).grid(row=1,columnspan=2,sticky=tk.E)
        self.scrollbar.config(command=self.text_box.yview)


class RootWindow(tk.Tk):
    """Main application interface"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title("R-Explore")
        self.geometry("505x655") # set so it isn't shrunken before loading tabs
        self.resizable(width=False,height=False)
        self.disk = app.get_disk() # get reference to disk
        self.protocol("WM_DELETE_WINDOW", self._on_quit) # make sure cleanup happens on window close
        self.taskbar_icon = tk.PhotoImage(file=ICON_32)
        self.call('wm', 'iconphoto', self._w, self.taskbar_icon)

        # bindings for main menu
        self.menu_callbacks = {'file->open': self._on_open,
                               'file->exportall': self._on_export_all,
                               'file->exportreport': self._on_export_report,
                               'file->quit': self._on_quit,
                               'help->about': self._on_about,
                               'help->view': self._on_help}
        # create main menu
        self.menubar = menus.MainMenu(self,self.menu_callbacks)
        self.config(menu=self.menubar)
    
        # create tabbed interface to show disk data
        self.tab_box = TabBox(self)
        self.tab_box.pack(fill=tk.BOTH, expand=1)
        
        # create status label at the bottom for currently open disk file
        self.file_label = tk.StringVar()
        self.file_label.set('No file open.')
        file_display = tk.Label(self,textvariable=self.file_label)
        file_display.pack(side=tk.BOTTOM,anchor=tk.W)
        

    
    def _on_export_all(self):
        """Open a file chooser and export all samples to a zip file."""
        filename = asksaveasfilename(initialdir=app.home_path,
                                     defaultextension='.zip',
                                     filetypes = (("Compressed archive (ZIP)", "*.zip"),),
                                     title="Choose a file")
        
        if filename != '':
            pw = w.ProgressWindow()
            pw.grab_set() # grab focus
            pw.transient(self) # keep on top of other app windows
            # Export the samples, and pass along progress bar callback
            total = app.get_disk().get_wave_count()
            app.export_all(filename,callback=pw.update_progress,steps=32) # 32 tones, 32 steps
            
    def _on_export_report(self):
        """Callback for exporting a disk report."""
        filename = asksaveasfilename(initialdir=app.home_path,
                                     defaultextension='.txt',
                                     filetypes=(("Text file", "*.txt"),),
                                     title="Choose a file")
        
        if filename != '':
            app.get_disk().export_disk_report(filename)


    def _on_quit(self):
        self.destroy()
        app.cleanup() # won't get called if app just dies, see if this can be caught

    def _on_about(self):
        """Display an 'about' dialog for the application."""
        aw = AboutWindow()
        aw.transient(self) # keep on top of other app windows
        
    def _on_help(self):
        hv = HelpViewer()
        hv.mainloop()
        
    def _close_disk(self):
        """Close a disk file and clean up."""
        

    def _on_open(self):
        """Open a new disk image."""
        new_filename = askopenfilename(initialdir=app.home_path,
                                       filetypes =(("Sample Disk Image", ("*.OUT","*.s55")),
                                                   ("All Files","*.*")),
                                       title="Choose a file")
        
        print(f'New filename: {new_filename}')
        # don't continue if no file was chosen
        if new_filename == '':
            return
        try:
            app.new_disk(new_filename)
        except FormatError as E:
            print(E)
            print('DEBUG: Unable to open new disk.')
            messagebox.showerror(title="File error",
                                 message="Invalid or unsupported disk format!",
                                 detail=str(E))
        except FileSizeError as E:
            print(E)
            messagebox.showerror(title="File error",
                                 message="File is too large to open!",
                                 detail=str(E))
        else:
            if not app.disk_is_empty():
                app.close_disk() # if there is already a disk open, clean up after it first
            self.disk = app.get_disk() # get reference to new disk instance
            self.file_label.set(f'Current file: {os.path.basename(new_filename)}') # update status display
            self.tab_box.cleanup() # test
            self.tab_box.destroy() # destroy old tab box so it's no longer displayed
            self.tab_box = TabBox(self) # create a new TabBox for the new data
            self.tab_box.pack(fill=tk.BOTH, expand=1)
            self.tab_box.enable() # enable TabBox
            
            # enable menu entries that are disabled on startup.
            # First argument to entryconfig is a positional index
            self.menubar.file_menu.entryconfig(1,state='normal') # Export All Samples
            self.menubar.file_menu.entryconfig(2,state='normal') # Export Disk Report
            

