R-Explore - A sample disk browser for Roland 12-bit samplers
------------------------------------------------------------

========================
Features
========================
- Play back wave samples
- Export single wave sample, or export all samples to a zip archive
- Export a text report containing parameter information
- Browse parameters (see 'Format Support' section)

========================
What It Doesn't Do
========================
- Edit disk data
- Read in disk images; a sample disk must first be imaged with another program
  such as SDISKW or dd.
- Currently, certain functions on certain samplers; see format support

========================
Usage
========================
Note: S-50 disks will display a limited interface; see 'Format Support' section.

When R-Explore first starts, most of the interface is disabled. Open a disk
image with 'File -> Open Disk Image' or Ctrl+O. The current disk image will be
displayed at the bottom of the main window. The main interface is split into
four tabs:

Function: This corresponds to data which is displayed in the Play mode of the
sampler, Function or Disk mode, including the Disk Label.

MIDI: This corresponds to data displayed in the MIDI mode of the sampler.

Patches: This displays a list of patches and their names and numbers, with
buttons to launch a window to browse their associated parameter data. Since
patches can contain multiple tones, there is no play function for this tab.

Tones: Similarly to the patch browser, this displays a list of tones and
buttons to open a window for browsing their parameters. Additionally, this tab
includes play buttons which work as a sound board for previewing tone samples.
At the bottom of a tone's window, there is an export button which can export a
single tone's wave data to a WAV file (16-bit, 44.1kHz).

All samples can be exported at once to a zip archive with the menu option
'File -> Export All Samples'.

The menu option 'File -> Export Disk Report' will output parameters from disk
to a text file; note that this reflects how the parameters are read into the
program, which is fairly close to how they're stored on disk. So some
parameters will not reflect what's displayed on the sampler, as they're stored
numbered from 0, etc.

========================
Keyboard Shortcuts
========================
Ctrl + O: Open disk image
Ctrl + E: Export all samples
Ctrl + S: Export disk report
Ctrl + Q: Quit R-Explore
Ctrl + H: Display this help

========================
Platform Support
========================
R-Explore is still in early development; it was developed on Linux Mint 20 with
Python 3.8, so support is mainly verified for this platform. However, it has
tested fine on Windows and macOS and should run on any major platform which
supports a recent version of Python. The only library dependency is
simpleaudio. R-Explore does not yet integrate well with macOS.

========================
Format Support
========================
Currently, R-Explore fully supports S-550 and S-330 disks. S-50 disks are
partially supported; the ability to play and export wave samples is included
but most parameters are not browsable. However, a text-based disk report can
be created to display the parameters, although the correctness of the report
has not been verified for this format. W-30 disks, or those of any other
samplers are not currently supported.

========================
Copying and Author
========================
This program is released under the terms of the MIT License and comes with no
warranty. For more information, see the LICENSE file. This program still needs
extensive testing, so if you run into any issues or have questions contact
me at laurenc.istrad@gmail.com, especially if you have an S-50.
