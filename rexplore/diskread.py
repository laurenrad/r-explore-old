"""
R-Explore: diskread.py
This module contains classes representing the data on a 12-bit sample disk
and associated methods for reading and exporting this data.
This module serves as the primary backend for R-Explore.
"""

import struct
import wave
import audioop
import os
from rexplore.exceptions import *
import rexplore.formats as formats
from rexplore.formatting import display_names
# quick notes: run through pycodestyle, mypy

# Globals
# Maximum filesize to prevent loading excessively large files into memory.
# This is set to 2MB to give some leeway to check the format
MAX_FILE_SIZE = 2000000

class Tone:
    """
    This class represents parameter data for a single tone.
    For now at least, this does not store wave data, because all wave data is
    stored together at the end of the disk and storing it per tone would
    increase complexity a great deal. Instead, wave data is stored in a disk
    object.
    """

    def __init__(self,img,fmt,number):
        """
        Initialize a Tone object from a tone number and a bytes object
        containing a disk image.
        """
        self.number = number
        BASE_ADDRESS =  fmt.TONES_OFFSET # starting address of tone parameter data
        TONE_SIZE = fmt.TONE_SIZE # each tone occupies 128 bytes on disk
        tone_address = BASE_ADDRESS + TONE_SIZE*self.number # calculate tone address
        self.values = {} # general parameter data
        b = DiskBytes(img,tone_address) # start at offset of this particular tone

        # Read the parameter data in according to the format definition
        # Temporarily hardcoded to S550 until further implementation
        b.read_params(self.values, fmt.tone_fmt)

        # The sample rate / sampling frequency parameter is actually used by
        # the backend, so it will be converted and stored as a usable value.
        # 0 = 30KHz, 1 = 15KHz
        self.values['FREQUENCY'] = 15000 if self.values['FREQUENCY'] else 30000
    
    def get_name(self) -> str:
        """Return the sample's name."""
        return self.values['NAME']
    
    def length(self):
        """Return the length of the tone in wave segments (0-18)."""
        return self.values['WAVE SEGMENT LENGTH']

class Patch:
    """
    This class represents parameter data for a single patch.
    """
    
    def __init__(self,img,fmt,number):
        """
        Initialize a Patch object from a patch number and a bytes object
        containing a disk image.
        """
        # Members for entire block
        self.number = number
        BASE_ADDRESS = fmt.PATCHES_OFFSET # starting address of block
        PATCH_SIZE = fmt.PATCH_SIZE # size in bytes of a single patch
        patch_address = BASE_ADDRESS + PATCH_SIZE*self.number # location of this patch
        b = DiskBytes(img,patch_address) # function parameters start at offset 68608
        self.values = {}

        # Read the parameter data in according to the format definition
        # Temporarily hardcoded to S550 until other formats are implemented.
        b.read_params(self.values, fmt.patch_fmt)
        
    def get_name(self) -> str:
        """Return the name of the patch."""
        return self.values['NAME']

class FunctionParams:
    """
    This class represents global function parameters for a sample disk.
    """
    
    # This and the below class are now a little bit empty since restructuring
    # but are being kept as classes for the time being.
    def __init__(self, img, fmt):
        """
        Initialize a FunctionParams object from a bytes object containing a
        disk image.
        """
        
        self.values = {} # the actual values retrieved from the disk image
        b = DiskBytes(img,68608) # function parameters start at offset 68608
        
        # Read the parameter data in according to the format definition
        # Temporarily hardcoded to S550 until other formats are implemented.
        b.read_params(self.values, fmt.func_fmt)
    
class MidiParams:
    """
    This class represents global MIDI parameters for a sample disk.
    """
    def __init__(self, img, fmt):
        """
        Initialize a MidiParams object from a bytes object containing a
        disk image.
        """
        b = DiskBytes(img,68832) # midi parameters start at offset 68832
        self.values = {}

        # Read the parameter data in according to the format definition
        # Temporarily hardcoded to S550 until further implementation
        b.read_params(self.values, fmt.midi_fmt)
        
class DiskBytes:
    """
    This is a class to provide a buffer to a bytes object representing
    a sample disk image.
    
    This class implements a pop function, making it easy to read through a
    disk image stored in a bytes object and interpret one parameter at a time
    without having to open the file again or keep track of the offset.
    
    Additional methods relevant to reading bytes off disk can be implemented
    as needed.
    """

    def __init__(self,bytesobject,offset=0):
        """Constructor takes a bytes object and an optional start offset."""
        self._view = memoryview(bytesobject[offset:])

    def pop(self,n: int) -> bytes:
        """Pop n bytes from contents and return them."""
        # store the bytes to be popped off and advance the view n bytes
        result, self._view = (self._view[:n].tobytes(),
                              self._view[n:]) 
        return result # return the popped bytes
    
    def skip(self,n: int):
        """Skip ahead n bytes in the buffer."""
        self._view = self._view[n:] # advance the view n bytes

    def contents(self) -> bytes:
        """Return all remaining buffer contents as a bytes object."""
        return self._view.tobytes()
    
    def read_params(self, values: dict, fmt: tuple):
        """
        Reads parameter data in according to the format definition provided
        and places it in the passed in values dict.
        """
        
        for param in fmt:
            pname = param['name']
            if param['type'] == 'int':
                values[pname] = int.from_bytes(self.pop(param['size']),byteorder='big',signed=False)
            elif param['type'] == 'signed_int':
                values[pname] = int.from_bytes(self.pop(param['size']),byteorder='big',signed=True)
            elif param['type'] == 'multi':
                fmt = str(param['size']) + 'b' # format string for struct.unpack
                values[pname] = struct.unpack(fmt,self.pop(param['size']))
            elif param['type'] == 'ascii':
                values[pname] = self.pop(param['size']).decode('ascii')
            elif param['type'] == 'raw':
                # for values that need to be unprocessed for formatting
                values[pname] = self.pop(param['size'])
            elif param['type'] == 'dummy':
                self.skip(param['size'])
            else:
                pass # TODO error handling

class Disk:
    """
    This class represents a 12-bit sample disk and provides method(s) to
    extract its wave data.
    """
    

    def __init__(self, filename):
        """
        Create a Disk object from an image file specified in a given filename.
        This will also create associated Tone, Patch, MidiParams, and
        FunctionParams objects.
        """
        self.patches = [] # list of patches
        self.tones = [] # list of tones
        self.filename = filename # name of disk image file
        
        # Check for excessively large file first
        if os.path.getsize(filename) > MAX_FILE_SIZE:
            raise FileSizeError()

        with open(filename,'rb') as disk:
            # read in the full image for params, but it's easier to get wave
            # and system data from the file while it's open here
            self.img = disk.read() # full image
            
            # an exception will be raised here if this fails
            self.format = formats.check(self.img) # check disk format
            
            if len(self.img) != 737280:
                print(f'Bad disk image size: {len(self.img)} bytes.')
            disk.seek(0) # back to the start to get system
            
            # system data
            self.sys = disk.read(64512) # at start of disk
            print(f'Read {len(self.sys)} bytes system data.')
            
            # wave data
            disk.seek(73728) #  wave data starts here
            self.wave = disk.read(663552)
            print(f'Read {len(self.wave)} bytes wave data.')
            
        # Function data
        self.function = FunctionParams(self.img,self.format) # function parameters
        print(f'Read function parameters from disk.')
        
        # MIDI data
        self.midi = MidiParams(self.img,self.format) # midi parameters
        print(f'Read MIDI parameters from disk.')
        
        # Patches
        for i in range(0,self.format.PATCH_COUNT):
            self.patches.append(Patch(self.img,self.format,i))
        print(f'Read {len(self.patches)} patches from disk.')

        # Tones
        for i in range(0,self.format.TONE_COUNT):
            self.tones.append(Tone(self.img,self.format,i))
        print(f'Read {len(self.tones)} tones from disk.')
        print('Done reading disk.')
        
    def get_wave_count(self) -> int:
        """
        Return the number of samples (non-empty tones) on a disk.
        """
        total = 0
        for tone in self.tones:
            if tone.length() != 0:
                total += 1
        return total
        
    def export_disk_report(self, filename:str):
        """
        Export a report containing unformatted parameter info to a text
        file. This will not export dummy data which is not read in.
        """
        with open(filename, 'w') as outfile:
            outfile.write("Function Data:\n")
            for key,value in self.function.values.items():
                outfile.write(f"{key}:  {value}\n")
            
            outfile.write("\nMIDI Data:\n")
            for key,value in self.midi.values.items():
                outfile.write(f"{key}:  {value}\n")
                
            outfile.write("\nPatch Data:\n")
            for patch in self.patches:
                for key,value in patch.values.items():
                    outfile.write(f"{key}:  {value}\n")
                outfile.write("\n")
                
            outfile.write("\nTone Data:\n")
            for tone in self.tones:
                for key,value in tone.values.items():
                    outfile.write(f"{key}:  {value}\n")
                outfile.write("\n")
        
    def create_wav(self,num: int,filename: str='tmp.wav'):
        """
        Encode a wav file from a given tone's wave data.
        Outputs to tmp.wav if optional filename is not specified.
        """
        # some constants
        SEGMENT_SIZE = 18432 # number of bytes in a segment
        BANK_SIZE = 331776  # number of bytes in a wave bank
        FRAMERATE_OUT = 44100 # output sample rate
        WIDTH_OUT = 2 # 16-bit (2 bytes)
        CHANNELS_OUT = 1 # mono
        
        tone_bank = self.tones[num].values['WAVE BANK']
        tone_seg = self.tones[num].values['WAVE SEGMENT TOP']
        tone_length = self.tones[num].length()
        tone_freq = self.tones[num].values['FREQUENCY']  
        
        # calculate location of tone's wave data on disk
        tone_addr = 0 # base address of wave data
    
        if tone_bank == 1: #if bank B, add size of bank A (330156 bytes)
            tone_addr += BANK_SIZE

        tone_addr += (SEGMENT_SIZE * tone_seg) # 18342 bytes per segment, 18 segments per bank
        #print(f'Tone address should be: {tone_addr}')
        end_addr = tone_addr + (SEGMENT_SIZE * tone_length) # address of next segment after tone
        
        wave_view = DiskBytes(self.wave,tone_addr) # memoryview access to wave data
        
        with wave.open(filename,'wb') as wav:
            wav.setnchannels(CHANNELS_OUT) # set number of output channels
            wav.setsampwidth(WIDTH_OUT) # set output sample width
            wav.setframerate(FRAMERATE_OUT) # set output sample rate
            
            frag_state = None # fragment state for audioop.ratecv to pass to next call
            while tone_addr < end_addr:
                # wave data is stored as 3-byte chunks, each containing two 12-bit samples
                # in format: A1 A2 A3 B3 B1 B2
                # samples will be shifted left 4 places so 16-bit values are output.

                rawbytes = wave_view.pop(3) # get a chunk
                # make sure we are not getting a partial chunk as this would throw things off
                if len(rawbytes) != 3:
                    print(f'Bad chunk size while reading wave data, got chunk of size {len(rawbytes)}')
                    continue # try another
                    
                chunk = int.from_bytes(rawbytes, byteorder='big', signed=False)
                # ...
                # for first sample, just mask off lower 12 bits and shift down
                sample_a = (chunk & 0xfff000) >> 8

                # for second, mask off all but lower 12 bits...
                chunk &= 0xfff
                # and move upper 8 bits into position, and set rest to 0
                sample_b = chunk << 8 & 0xff00
                # finally, move lower 4 bits into position in original variable and set rest to 0
                # then, logical 'or' it with the upper 8 to put everything together
                sample_b |= (chunk >> 4 & 0xf0)

                sample_pack = struct.pack('2H',sample_a,sample_b) # pack as unsigned shorts
                
                # for the first sample, frag_state is 'None' which is fine
                # ratecvreturns a tuple with resampled fragment and state to
                # pass to the next call
                (resample,frag_state) = audioop.ratecv(sample_pack,WIDTH_OUT,
                                                       CHANNELS_OUT,tone_freq,
                                                       FRAMERATE_OUT,frag_state)
                wav.writeframes(resample)
                tone_addr += 3 # just to keep track of where we are

if __name__ == '__main__':
    d = Disk('SATAN.s55') # create a test disk