# R-Explorer: patch_gui.py
# Patch detail view window

import tkinter as tk
import rexplore.widgets as w
import rexplore.formatting as formatting
from rexplore.formatting import display_names # Display names of parameters
import rexplore.app as app

class PatchViewer(tk.Toplevel):
    """Displays information about a single patch at a time."""
    def __init__(self,num,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.number = num
        self.values = app.get_disk().patches[num].values.copy() # get shallow copy
        
        # set window properties
        self.title("Patch View")
        self.minsize(330,0) # needs to be at least 330 px wide
        self.resizable(width=False,height=False)
        
        params_frame = w.ParamsFrame(self) # create frame to display parameters
        params_frame.grid_columnconfigure(0,weight=1,minsize=100) # grow columns
        for (key,value) in self.values.items():
            if key not in formatting.Fields550.patch_nodisplay:
                params_frame.add(key,value) # add parameter to the frame
        params_frame.pack(fill=tk.BOTH, expand=1, padx=5)
            
        # This is a temporary display of the tone to key values until something fancier
        # can be implemented.
        # By the way, D8 is the highest value the interface actually seems to interact with,
        # but more is stored so since this is temporary it will just do the whole thing.
        split_frame = tk.LabelFrame(self,text="Tone Assignment")
        note_values = []
        for i in range(12,121):
            note_values.append(formatting.int_to_note(i)) # create a list of possible note values
        
        self.tone1 = tk.StringVar() # variable for 1st tone assigned to a note
        self.tone2 = tk.StringVar() # variable for 2nd tone assigned to a note
        self.note_select = tk.Spinbox(split_frame,values=note_values,
                                      command=self._on_note_select,state='readonly',
                                      wrap=True)
        self.note_select.grid(row=0,columnspan=2)
        tk.Label(split_frame,text="1st tone: ").grid(row=1,column=0,sticky=tk.W)
        tk.Label(split_frame,textvariable=self.tone1).grid(row=1,column=1,sticky=tk.E)
        tk.Label(split_frame,text="2nd tone: ").grid(row=2,column=0,sticky=tk.W)
        tk.Label(split_frame,textvariable=self.tone2,width=12).grid(row=2,column=1,sticky=tk.E)
        
        split_frame.pack(fill=tk.BOTH, expand=1,padx=5)
        tk.Button(self,text="Close",command=self.destroy).pack()
        
    def _on_note_select(self):
        """Callback for the temporary tone to key widget."""
        note = self.note_select.get()
        index = formatting.note_to_int(note)-12 # index of tone to key value
        tone_num1 = self.values['TONE TO KEY 1'][index] # index of the first tone assigned
        tone_num2 = self.values['TONE TO KEY 2'][index] # index of the first tone assigned
        self.tone1.set(formatting.roland_number(tone_num1) + ' ' + app.get_tone_name(tone_num1))
        self.tone2.set(formatting.roland_number(tone_num2) + ' ' + app.get_tone_name(tone_num2))
        
