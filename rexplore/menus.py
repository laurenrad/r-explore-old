"""
R-Explore: menus.py
Module for toplevel menus.
"""
import tkinter as tk
from functools import partial

class MainMenu(tk.Menu):
    """Toplevel menu bar."""
    def __init__(self, parent, callbacks, **kwargs):
        super().__init__(parent,**kwargs)
        
        # store callbacks
        self.callbacks = callbacks
        self._bind_accelerators() # bind key accelerators
        
        # create toplevel
        self.file_menu = tk.Menu(self, tearoff=False)
        self.help_menu = tk.Menu(self, tearoff=False)
        
        # add file menu options
        self.file_menu.add_command(label='Open Disk Image',command=callbacks['file->open'],
                                   accelerator='Ctrl+O')
        self.file_menu.add_command(label='Export All Samples',command=callbacks['file->exportall'],
                                   accelerator='Ctrl+E', state=tk.DISABLED) # only enable if a disk is open
        self.file_menu.add_command(label='Export Disk Report',command=callbacks['file->exportreport'],
                                   accelerator='Ctrl+S', state=tk.DISABLED) # same as above
        self.file_menu.add_separator()
        self.file_menu.add_command(label='Quit',command=callbacks['file->quit'],accelerator='Ctrl+Q')
        
        # add help menu options
        self.help_menu.add_command(label='View help',command=callbacks['help->view'],
                                   accelerator='Ctrl+H')
        self.help_menu.add_command(label='About R-Explore',command=callbacks['help->about'])
        
        # add all to toplevel
        self.add_cascade(label="File", menu=self.file_menu)
        self.add_cascade(label="Help",menu=self.help_menu)
        
    @staticmethod
    def _argstrip(function, *args):
        """Strip arguments from callbacks."""
        return function()
    
    def get_keybindings(self):
        """Return a dict containing global key bindings."""
        return { '<Control-o>': self.callbacks['file->open'],
                 '<Control-e>': self.callbacks['file->exportall'],
                 '<Control-s>': self.callbacks['file->exportreport'],
                 '<Control-q>': self.callbacks['file->quit'],
                 '<Control-h>': self.callbacks['help->view'] }
    
    def _bind_accelerators(self):
        """Bind global key shortcuts."""
        keybinds = self.get_keybindings()
        for key, command in keybinds.items():
            self.bind_all(key, partial(self._argstrip, command))