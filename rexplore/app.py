# possible simpler alt version for application.py
# Lauren C 2020
# Depends: simpleaudio
import simpleaudio as sa
import sys
import os
import tempfile
import pathlib # for export path
from zipfile import ZipFile
from rexplore.diskread import Disk
from rexplore.exceptions import *
import _thread as thread


def get_disk():
    """Return the current instance's disk."""
    return __disk

def get_disk_label():
    """Convenience accessor for returning the disk label."""
    return __disk.function.values['DISK LABEL']

def get_tone_name(num: int):
    """Get a tone's name from its index."""
    return __disk.tones[num].get_name()

def new_disk(filename: str):
    """Open a new disk image."""
    global __disk
    try:
        __disk = Disk(filename)
    except Exception as E:
        print(f"DEBUG: new_disk caught exception: {E}")
        raise E # pass up to interface
    
def close_disk():
    """Clean up without removing temporary directory."""
    # clean up temporary wave files
    for i in range(0,32):
        fname = __tempdir.name + os.sep + f'{i}.wav'
        if os.path.exists(fname):
            os.remove(fname)

def export_tone(num: int, *args):
    """Export a tone's wave data"""
    # for now, give up if empty. this checking is also done in extract_wave,
    # but this should be replaced later anyway [TMP]
    
    if __disk.tones[num].length() == 0:
        print("DEBUG: export_tone(): Empty tone!")
    else:
        __disk.create_wav(num, *args) # create a WAV file from the tone
            
def export_all(filename: str, callback=None, **cb_kwargs):
    """
    Export all samples from disk and compress to a zipfile.
    Callback is a callback function which updates on progress, and will
    receive any additional keyword args.
    """

    # For each possible tone, export a wave file and add it to the zip file
    with ZipFile(filename, 'w') as zipfile:
        for i in range(0,__disk.format.TONE_COUNT):
            if __disk.tones[i].length() != 0:
                path_temp = __tempdir.name + os.sep + f'{i}.wav' # path of temporary file
                path_out = 'samples' + os.sep + f'{i}.wav' # path of output file in archive
                export_tone(i,path_temp)
                # arcname is the name within the archive. this prevents
                # the archive from containing the temp directory
                zipfile.write(path_temp,arcname=path_out)
            if callback:
                callback(**cb_kwargs)
        
def disk_is_empty() -> bool:
    """Return True if no disk is loaded, False otherwise."""
    return __disk is None
        
def play_file(filename: str):
    """Play a WAV file using simpleaudio."""
    wave_obj = sa.WaveObject.from_wave_file(filename)
    play_obj = wave_obj.play()
    play_obj.wait_done()
    
def play(num):
    """Play a tone wave."""
    outfile = __tempdir.name + os.sep + f'{num}.wav'
    if not os.path.exists(outfile):
        export_tone(num,outfile) # only create the file if it doesn't already exist
    thread.start_new_thread(play_file,(outfile,))
    
def get_tempdir():
    return __tempdir
    
def cleanup():
    """
    Clean up temp files after exiting.
    """
    global tempdir
    __tempdir.cleanup()
    print(f"Cleaned up temporary directory.")

    
# Globals section
__disk = None # global disk
# TemporaryDirectory() allows the use of context managers, but for now
# it's best to just let it persist and err on the side of leaving files
# so temporary files can be reused during execution.
__tempdir = tempfile.TemporaryDirectory()
print(f"Created temporary directory {__tempdir.name}")
version = '0.5 (RC1)' # version string
home_path = pathlib.Path.home()