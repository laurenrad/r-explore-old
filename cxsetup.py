import cx_Freeze as cx
import platform
import os

include_files = [('rexplore/images', 'images'),('rexplore/doc', 'doc')]
base = None
target_name = 'rexplore'

if platform.system() == "Windows":
    # Fix for issues in Python 3.6 and cx_Freeze
    PYTHON_DIR = os.path.dirname(os.path.dirname(os.__file__))
    os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_DIR, 'tcl', 'tcl8.6')
    os.environ['TK_LIBRARY'] = os.path.join(PYTHON_DIR, 'tcl', 'tk8.6')
    include_files += [
        (os.path.join(PYTHON_DIR, 'DLLs', 'tcl86t.dll'), ''),
        (os.path.join(PYTHON_DIR, 'DLLs', 'tk86t.dll'), '')]
    
    # Set target and base for Windows
    base = "Win32GUI"
    target_name = 'rexplore.exe'


cx.setup(
    name='R-Explore',
    version='0.5.0',
    author='Lauren Croney',
    author_email='laurenc.ist.rad@gmail.com',
    description='Sample disk browser for Roland 12-bit samplers',
    url="https://bitbucket.org/laurenrad/r-explore-rc1",
    packages=['rexplore'],
    executables=[
        cx.Executable('r_explore.pyw', base=base,
                      targetName='rexplore', icon='rexplore.ico')],
    options={
        'build_exe': {
            'packages': ['simpleaudio'],
            #'includes': ['wave', 'audioop', 'zipfile']
            'include_files': include_files
        }
    })
