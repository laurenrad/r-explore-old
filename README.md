R-Explore
================================================
A sample disk browser for Roland 12-bit samplers

Features
--------
    * Play back wave samples
    * Export single wave sample, or export all samples to a zip archive
    * Export a text report containing parameter information
    * Browse parameters (see 'Format Support' section)

What It Doesn't Do
------------------
    * Edit disk data
    * Read in disk images; a sample disk must first be imaged with another program such as SDISKW or dd.
__Note: S-50 disks will display a limited interface; see 'Format Support' section.__

Format Support
--------------
Currently, R-Explore fully supports S-550 and S-330 disks. S-50 disks are partially supported; the ability to play and export wave samples is included but most parameters are not browsable. However, a text-based disk report can be created to display the parameters, although the correctness of the report has not been verified for this format. W-30 disks, or those of any other samplers are not currently supported.

Copying
-------
This program is released under the terms of the MIT License and comes with no
warranty. For more information, see the LICENSE file.
