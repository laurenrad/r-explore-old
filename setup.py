from setuptools import setup, find_packages

# TODO: add long_description, test with lower python versions,
# check library version requirements
setup(
    name='R-Explore',
    version='0.5.0',
    author='Lauren Croney',
    author_email='laurenc.ist.rad@gmail.com',
    description='Sample disk browser for Roland 12-bit samplers',
    url="https://bitbucket.org/laurenrad/r-explore-rc1",
    license='MIT',
    # Add long_description rst later
    packages=find_packages(),
    install_requires=['simpleaudio'],
    python_requires='>= 3.6',
    package_data={'rexplore.images': ['*.png'], 'rexplore.doc': ['*.txt']},
    entry_points={'console_scripts': ['rexplore = rexplore:main']}
)
